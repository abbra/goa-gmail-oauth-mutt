GNOME Online Accounts client to provide GMail OAuth2 for mutt
=============================================================

GMail provides a traditional IMAPS service for accessing mail, however,
instead of using a password based authentication scheme, it recommends use
of the [OAuth2 protocol](https://oauth.net/2/).

The mutt mail client has support for OAuth2, however, the commonly
documented procedure for configuring this requires many complex steps
to create a Google Cloud API application to in turn enable access tokens
to be requested. This results in a poor user experiance for people
configuring mutt with GMail.

The [GNOME Online Accounts](https://wiki.gnome.org/Projects/GnomeOnlineAccounts)
service provides a [DBus API](https://developer.gnome.org/goa/stable/)
for requesting OAuth2 access tokens from Google. Making use of this
service allows for a much simpler mutt setup, avoiding the need to deal
with the Google Cloud API directly.

If you don't already have a Google Account configured in GNOME Online
Accounts, then follow the [simple setup steps](docs/setup-goa.md) first.

The script makes use of the Python3 GObject Introspection bindings, so
make sure you have the package installed:

```
 # dnf install python3-gobject (on Fedora/RHEL/CentOS)
 # apt-get install python3-gi  (on Debian/Ubuntu)
```

To check that everything is working, invoke the script with the ``--list``
argument.  It will print the identities of the GMail accounts that are
configured:

```
$ ./goa-gmail-oauth-mutt.py --list
```

Now copy the ``goa-gmail-oauth-mutt.py`` script to ``$HOME/.local/bin``
and add the following to your ``$HOME/.muttrc``

```
  set imap_user = "YOUR@GOOGLE-ACCOUNT-EMAIL.COM"
  set imap_authenticators = "oauthbearer"
  set imap_oauth_refresh_command = "~/.local/bin/goa-gmail-oauth-mutt.py"
  set folder = "imaps://imap.gmail.com:993"
  set spoolfile = "+INBOX"
  set postponed = "+[Gmail]/Drafts"

  set smtp_url = "smtp://YOUR@GOOGLE-ACCOUNT-EMAIL.COM@smtp.gmail.com:587"
  set smtp_authenticators = "oauthbearer"
  set smtp_oauth_refresh_command = "~/.local/bin/goa-gmail-oauth-mutt.py"
  set from = "YOUR@GOOGLE-ACCOUNT-EMAIL.COM"
  set realname = "YOUR REAL NAME"
  set use_from = yes
```

When mutt connects to GMail's IMAP or SMTP server, it will invoke the
``goa-gmail-oauth-mutt.py`` script to acquire an access token transparently.

If multiple Google accounts are configured in GNOME Online Accounts, the
``goa-gmail-oauth-mutt.py`` script will return the first account it finds.
To reliably select a specific account, pass the email address associated
with the account as the first command line parameter to the script.
