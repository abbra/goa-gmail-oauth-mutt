Setting up a Google Account with GNOME Online Accounts
======================================================

If you don't already have a Google account setup in GNOME Online Accounts,
follow this simple guide:

 1. Open the GNOME Settings application and select "Online accounts"

    ![](goa-add-account.png)


 2. Select "Google" under "Add an account" and enter the
    email address associated with your account

    ![](goa-add-google.png)


 3. After authenticating with your account, it will ask to
    confirm grant of many permissions to GNOME

    ![](goa-approve-gnome-request.png)


 4. There is no way to tailor this permissions list, so scroll
    down and allow it

    ![](goa-approve-gnome-allow.png)


 5. There are many features in GNOME that can integrate with
    your Google account. Initially all are enabled, but for
    the purposes of IMAPS it can optionally be cut down to
    just the Mail feature.

    ![](goa-select-gnome-services.png)


 6. After confirming the integration features, your Google
    account is now ready for use

    ![](goa-accounts-list.png)
